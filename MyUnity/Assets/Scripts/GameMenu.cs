﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour
{
    public void StartGameButton()
    {
        SceneManager.LoadScene("Scene");
    }

    public void ExitButton()
    {
        Application.Quit();
    }
}
