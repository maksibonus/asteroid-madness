﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject hazard;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public GameObject player;

    public GUIText scoreText;
    public GUIText restartText;
    public GUIText gameOverText;
    public GUIText quitText;
    public GUIText infoText;

    public int lives;
    public Vector3 liveValues;
    public GameObject live;

    private int score;
    private bool gameOver;
    private bool restart;


    private string[] info;
    private int currentInfo;

    void Start()
    {
        info = new string[7];
        info[0] = "You need to go back to your planet.\n You must survive!";
        info[1] = "It was easy.";
        info[2] = "Why is shoot so slow?";
        info[3] = "Need save the shells!";
        info[4] = "Hang in there buddy!";
        info[5] = "We almost there!";
        info[6] = "I am kidding you! xD";
        currentInfo = 0;

        hazard.GetComponent<Mover>().speed = -5;

        gameOver = false;
        restart = false;

        infoText.text = "";
        restartText.text = "";
        gameOverText.text = "";
        quitText.text = "Press 'Q' to exit";

        score = 0;
        UpdateScore();

        StartCoroutine(SpawnWaves());
        RenderLive();
    }

    void Update()
    {
        if(restart)
        {
            if(Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(Application.loadedLevel);
            }
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Application.Quit();
        }
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            ChangeInfo();
            yield return new WaitForSeconds(waveWait);
            infoText.text = "";
            for (int i = 0; i < hazardCount; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                if (gameOver)
                {
                    restartText.text = "Press 'R' for Restart";
                    restart = true;
                    break;
                }
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
            ChangeAsteroids();
            ChangeAudioSpeed();
            ChangeFireRate();
            if (gameOver)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
        }
    }

    void RenderLive()
    {
        Vector3 livePosition = new Vector3(liveValues.x, liveValues.y, liveValues.z);
        Quaternion liveRotation = Quaternion.identity;
        GameObject newLive;
        for (int i = 0; i < lives; i++)
        {
            newLive = (GameObject)Instantiate(live);
            newLive.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform);
            newLive.GetComponent<RectTransform>().anchoredPosition3D = livePosition;
            newLive.GetComponent<RectTransform>().rotation = liveRotation;
            livePosition += new Vector3(28, 0, 0);
        }
    }

    private void ChangeInfo()
    {
        if (currentInfo < 7)
            infoText.text = info[currentInfo++];
        else
            infoText.text = "";
    }

    private void ChangeAsteroids()
    {
        hazardCount += 5;
        float speed = hazard.GetComponent<Mover>().speed;
        if (speed>-25f)
            hazard.GetComponent<Mover>().speed -= 4;
    }

    private void ChangeFireRate()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            if (player.GetComponent<PlayerController>().fireRate < 1.0f)
            {
                player.GetComponent<PlayerController>().fireRate += 0.15f;
            }
        }
    }

    private void ChangeAudioSpeed()
    {
        if (GetComponent<AudioSource>().pitch <= 2.01f)
            GetComponent<AudioSource>().pitch += 0.2f;

    }

    public void AddScore (int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    public void GameOver()
    {
        gameOverText.text = "Game Over";
        gameOver = true;
    }

    public void DecreaseLives()
    {
        lives--;
        GameObject[] liveObjects = GameObject.FindGameObjectsWithTag("Live");
        if (liveObjects != null)
        {
            for (int i=0;i<liveObjects.Length;i++)
            {
                GameObject.Destroy(liveObjects[i]);
            }
            RenderLive();
        }
        if (liveObjects == null)
        {
            print("Cannot find 'Live' object");
        }
        if (lives == 0)
        {
            GameOver();
            return;
        }
        Instantiate(player, transform.position, transform.rotation);
    }
}
