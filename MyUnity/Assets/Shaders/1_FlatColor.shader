﻿Shader "unityCookie/tut/Introduction/1 - FlatColor" 
{
	Properties
	{
		_Color("Color", Color) = (1.0,1.0,1.0,1.0)
	}
		SubShader
	{
		Pass
		{
			CGPROGRAM

			//pragmas

			//variables
			uniform float4 _Color;
			
			//input structs
			struct vertexInput
			{
				float4 vertex : POSITION;
			};
			struct vertexOutput
			{
				float4 pos : SV_POSITION;
			};

			//vertex function
			vertexOutput vert(vertexInput vi)
			{
				vertexOuput o;
				o = vertexOutput;
				return o;
			}
			//fragment function

			ENDCG
		}
	}
}
