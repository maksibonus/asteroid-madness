﻿Shader "Custom/HeightMapTexture" { //Имя шейдеры при выборе его на материале
	Properties{ //Блок с параметрами для Unity3D
		_HeightTex("HeightMap Texture", 2D) = "white" {} //Имя параметр, описание для редактора и тип. Далее идёт значение по умолчанию 
	_GrassTex("Grass Texture", 2D) = "white" {} //Тип 2D указывает что тут обычная текстура
	_RockTex("Rock Texture", 2D) = "white" {}
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" } //Теги для рендеринга
		LOD 200

		CGPROGRAM //Говорим что мы шпрехаем на CG и начинаем писать на нём
#pragma surface surf Lambert //Директива для доп. данных, таких как: Функция Surface шейдера и модель освещения.
		sampler2D _GrassTex; //Указываем переменные в которые будут переданные данные из блока Properties имена и типы желательно указывать такие же
	sampler2D _RockTex;
	sampler2D _HeightTex;
	struct Input { //Структура для входных данных для вызова шейдера, заполняется автоматически нам нужно только указать что хотим
		float2 uv_GrassTex; //uv для текстур например, позволит настраивать тайлинг и оффсет из редактора
		float2 uv_RockTex;
		float2 uv_HeightTex;
	};

	void surf(Input IN, inout SurfaceOutput o) { //Собственно функция сурфейс шейдера
												 //Получаем из текстур цвета пикселей по UV
		float4 grass = tex2D(_GrassTex, IN.uv_GrassTex); //Трава
		float4 rock = tex2D(_RockTex, IN.uv_RockTex); //Камень\Грязь
		float4 height = tex2D(_HeightTex, IN.uv_HeightTex); //Карта высот

		o.Albedo = lerp(grass, rock, height); //Интерполируем по карте высот цвета травы и камня и получаем итоговый цвет  
											  //И помещаем его в структуру SurfaceOutput которая нужна для передачи результата работы :)
	}
	ENDCG
	}
		FallBack "Diffuse" //Если что-то не так взять обычный дифуз
}