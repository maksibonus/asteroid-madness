﻿Shader "AgasperShaders/TestShader" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_SpecColor("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess("Shininess", Range(0.03, 1)) = 0.078125
		_MainTex("Base (RGB) Gloss (A)", 2D) = "white" {}
	_BumpMap("Normalmap", 2D) = "bump" {}
	_Amount("Extrusion Amount", Range(-1,1)) = 0.5
	}

		SubShader{
		Tags{ "RenderType" = "Opaque" } // не прозрачный объект будет рисовать

		CGPROGRAM
		//шейдер Diffuse Bumped Specular(с BlinnPhong освещением) с морфингом(плавный переход от одной картинки к другой)
		/*
			процедура отрисовки поверхности называется surf
			в шейдере будет использоваться свет типа BlinnPhong (еще бывает Lambert, Unlit или своя процедура)
			процедура изменения вертексов называется vert
		*/
#pragma surface surf BlinnPhong vertex:vert

		sampler2D _MainTex;
	sampler2D _BumpMap;
	fixed4 _Color;
	half _Shininess;
	float _Amount;

	void vert(inout appdata_full v) {  
		v.vertex.xyz += v.normal * _Amount * v.vertex.xyz;
	}

	/*
	float4 vertex — координаты текущего вертекса
float3 normal — нормаль к поверхности в текущей точке
float4 texcoord — UV координаты первой текстуры   
float4 texcoord1 — UV координаты второй текстуры  //хранимой в моделе!! ее не увидешь, пока не нанесешь текстуру
float4 tangent — тангенс вектор                   //тангента (касательная прямая) точки
float4 color — цвет вертекса
	*/


	struct Input {
		float2 uv_MainTex;
		float2 uv2_MainTex; // если моделер добавил
		float2 uv_BumpMap;
	};

	void surf(Input IN, inout SurfaceOutput o) {
		fixed4 tex = tex2D(_MainTex, IN.uv2_MainTex);
		o.Albedo = tex.rgb * _Color.rgb;
		o.Gloss = tex.rgb;
		o.Specular = _Shininess;
		o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
	} 
	/*
	struct SurfaceOutput {
		half3 Albedo; //Альбедо поверхности (Цвет)
		half3 Normal; //Нормаль поверхности
		half3 Emission; //Эмиссия (используется для расчета отражения)
		half Specular; //"Размытие" отблеска в данной точке (зависит от направления камеры (dot(viewDir, Normal))
		half Gloss; //Сила отблеска в данной точке
		half Alpha; //Прозрачность в данной точке (не будет использоваться в "RenderType" = "Opaque")
	};
	*/

	ENDCG
	}

		FallBack "Specular"
}